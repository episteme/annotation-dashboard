import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';

import Annotation from '../ui/Annotation.jsx';
import DocumentAnnotation from '../ui/DocumentAnnotation.jsx';

import MainNav from '../ui/MainNav.jsx';
import Sidebar from '../ui/Sidebar.jsx';

import config from 'config'

// App component - represents the whole app
export class GlossaryPage extends Component {
	terms(){
		let annotations = _(this.props.annotations).filter((annotation)=>{
			return annotation.tags.indexOf("+glossary") > -1
		}).value()

		// console.log(annotations)

		var terms= []

		annotations.map((annotation) => {
			//console.log(annotation)
			
			annotation.target[0].selector.map((selector)=>{
				//console.log(selector)
				//console.log(terms)	
				if(selector.type == "TextQuoteSelector"){
					terms.push(selector.exact.toLowerCase())
				}
			})
		})

		return terms.sort()
	}

	definitions() {
		let terms = this.terms(this.props.annotations)

		return _(terms).map((term)=>{
			
			let defs = _(this.props.annotations).filter((annotation) => {
				return annotation.tags.indexOf(term) > -1 && annotation.tags.indexOf("definition") > -1
			}).map((annotation)=>{
				return <Annotation annotation={ annotation } />
			}).value()

			let refs = _(this.props.annotations).filter((annotation)=>{
				var exact = "";

				for (var k in annotation.target[0].selector){
					let selector = annotation.target[0].selector[k]


					if (selector.type == "TextQuoteSelector") {
						exact = selector.exact.toLowerCase()
					}
				}

				// console.log("exact:", exact)

				return exact.includes(term) && annotation.tags.indexOf("+glossary") < 0
			}).map((annotation)=>{
				return <Annotation annotation={ annotation }/>
			}).value()
			
			return (
			<div className="mb-3">
				<h3 id="{ term  }">{ term }</h3>
				
				<h6 className="text-muted">Definitions</h6>
				
				{ defs }

				<h6 className="text-muted">References</h6>

				<div className="card-columns">
				{ refs }
				</div>

			</div>
			)
		}).value()
  }

	index() {
		let terms = this.terms()

		terms = _(terms).groupBy((term) => {
			return term.charAt(0)		
		})
	
		let index = terms.map((items, key)=>{
			let entries = _(items).map((term) => {
				return <li><a href="#{ term  }">{ term }</a></li>
			}).value()

			console.log(entries)

			return ( 
				<li>
					{ key }

					<ul className="p-0 mb-2">
						{ entries }
					</ul>
				</li>)
		}).value()

		return <ul id="index" className="sticky-top text-muted p-0 small">
			{ index }
			</ul>
	}

  render() {
    return (
      <IntlProvider locale="en">
      <div className="container">
				
				<h1 className="mb-5 mt-5">{ config.group.title }</h1>

				<MainNav />

        <div className="row">
          <div className="col col-3">
            { this.index() }
          </div>

          <div className="col col-9">
            { this.definitions() }
          </div>
        </div>
      </div>
    </IntlProvider>
    );
  }
}

GlossaryPage.propTypes = {
  annotations: PropTypes.array.isRequired,
};

export default connect(
	state => {
		return { annotations: state.annotations.items  }
	}
)(GlossaryPage)
