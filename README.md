** WORK IN PROGRESS **

[mockups](https://taniki.github.io/netrights-dashboard-mockup/)

A single page application using web annotations data to build a collective dashboard. The app expect a json from a remote server corresponding to the aggregation of hypothes.is API. 

Building blocks:

- react for the component
- redux for the dispatcher
- thunk for the async
- bootstrap for the semantic ui

## Configure

Edit `config/default.json`


## Develop

```
$ yarn install
$ yarn start
```

## Build

```
$ webpack
```

## Deploy

- copy `dist` content to any static web server root

[ ] write documentation on how to deploy on github pages

## Directory

Existing dashboards

- NetRights
- Astrophysique
- Web We Can Afford
